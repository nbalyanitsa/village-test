# README #

Source code for village test.

Focuses heavily on modularity and separation of concerns, using IoC.

### Nice things ###
Height map generator (editor sript)

Nested Build menu configuration(potential fo subcategories)

Isometric layout.

### Nice things left behind due to time limit###
Road sprites. Unfortunately only one sprite is used

### Things not done properly ###
external asset loading (configs etc are hardcoded in services/models)


### Usage ###
Editor: Please use "Main" scene. Run a generator script on Map object in edit mode (NOT runtime).
Runtime: Select obejct to place form bottom menu (its nested so, clicking on category opens a submenu).
Right click to cancel placement.
