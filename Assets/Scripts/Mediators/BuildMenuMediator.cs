﻿using System;
using Views.Api;
using Models.Api;
using Signals;
using strange.extensions.mediation.impl;
using System.Collections.Generic;

namespace Mediators
{
	public class BuildMenuMediator:Mediator
	{
		[Inject]
		public IDisplayBuildMenu view{ get; set; }

		[Inject]
		public IReadBuildMenu model{ get; set; }

		[Inject]
		public IReadSelectedGroupName selectedGroup{ get; set; }

		[Inject]
		public IReadPlacedObjects placedObjects{ get; set; }

		[Inject]
		public PopulateBuildMenuSignal update{ get; set; }

		public override void OnRegister ()
		{
			update.AddListener (UpdateView);
		}

		[PostConstruct]
		public void UpdateView (){
			List<NamedEntity> items = null;

			var groupName = selectedGroup.GetSelectedGroupName ();
			if (groupName == null)
				items = model.GetBuildMenu (); //showing root
			else {
				items = (model.GetItem (groupName) as NamedGroup).objects;
			}
			view.ShowBuildMenu (items);
		}

		override public void OnRemove (){
			update.RemoveListener (UpdateView);
		}
	}
}

