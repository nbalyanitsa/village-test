﻿using System;
using strange.extensions.mediation.impl;
using Models.Api;
using Views.Api;
using UnityEngine;
using Signals;

namespace Mediators
{
	public class PlacedObjectClickMediator:Mediator
	{
		[Inject]
		public IHandlePlacedObjectClick view{ get; set; }

		[Inject]
		public IReadPlacedObjects placedObjects{ get; set; }

		[Inject]
		public PlacedObjectClicked signal{ get; set; }

		public override void OnRegister ()
		{
			view.GetClickedSignal ().AddListener (OnClick);
		}

		void OnClick (Vector3Int pos){
			var obj = placedObjects.GetObjectAt (new Vector2Int (pos.x, pos.y));
			if (obj != null) {
				signal.Dispatch (obj);
			}
		}

		public override void OnRemove ()
		{
			view.GetClickedSignal ().RemoveListener (OnClick);
		}
	}
}

