﻿using System;
using Views.Api;
using Models.Api;
using strange.extensions.mediation.impl;

namespace Mediators
{
	public class HeightMapMediator:Mediator
	{
		[Inject]
		public IProvideHeightMap view{ get; set; }

		[Inject]
		public IWriteHeightMap model{ get; set; }

		public override void OnRegister ()
		{
			//WARNING, a view as a source of data for a model
			//this is generally a side effect of reusing the same scene for level editor and game.
			//ideally editor should use a different scene, write the heightmap etc into a file and then a new game scene initialised from it.

			model.SetHeightMap (view.GetHeigthMap ());
		}
	}
}

