﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;

namespace Mediators
{
	public class ItemPlacementMediator:Mediator
	{
		[Inject]
		public IPlaceSelectedBuildItem view { get; set; }

		[Inject]
		public PlaceItemSignal signal { get; set; }

		public override void OnRegister ()
		{
			view.GetPlacedSignal ().AddListener (OnPlaced);
		}

		void OnPlaced (){
			signal.Dispatch ();
		}

		public override void OnRemove ()
		{
			view.GetPlacedSignal ().RemoveListener (OnPlaced);
		}
	}
}

