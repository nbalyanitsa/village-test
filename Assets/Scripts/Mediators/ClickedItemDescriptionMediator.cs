﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;
using Models.Api;
using UnityEngine;

namespace Mediators
{
	public class ClickedItemDescriptionMediator:Mediator
	{
		[Inject]
		public IDisplayClickedItemDescription view{ get; set; }

		[Inject]
		public PlacedObjectClicked signal { get; set; }

		[Inject]
		public IReadPlacedObjects placedObjects { get; set; }

		[Inject]
		public IReadBuildMenu buildMenu { get; set; }

		public override void OnRegister ()
		{
			signal.AddListener (OnClick);
		}

		void OnClick (BuildObject obj){
			if (Time.time >  placedObjects.GetLastPlacementTime () + 0.1f) {
				view.ShowDescription (buildMenu.GetItemDescription(obj.name));
			}
		}

		public override void OnRemove ()
		{
			signal.RemoveListener (OnClick);
		}
	}
}

