﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;
using Models.Api;
using UnityEngine;

namespace Mediators
{
	public class MapObjectMediator:Mediator
	{
		[Inject]
		public IDisplayMapObject view{ get;set;}

		[Inject]
		public ItemPlacedSignal update{ get;set;}

		[Inject]
		public IReadPlacedObjects model{ get;set;}

		public override void OnRegister ()
		{
			update.AddListener (OnItemPlaced);
		}

		void OnItemPlaced(Vector2Int pos){
			var itemPos = view.GetPosition ();
			if (pos.x == itemPos.x && pos.y == itemPos.y) {
				RefreshObject ();
			}
		}

		[PostConstruct]
		public void RefreshObject(){
			var itemPos = view.GetPosition ();
			view.SetMapObject (model.GetObjectAt(new Vector2Int(itemPos.x, itemPos.y)));
		}

		public override void OnRemove ()
		{
			update.RemoveListener (OnItemPlaced);
		}

	}
}

