﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;

namespace Mediators
{
	public class ItemSelectionMediator:Mediator
	{
		[Inject]
		public ISelectBuildItem view { get; set; }

		[Inject]
		public SelectItemSignal signal { get; set; }

		public override void OnRegister ()
		{
			view.GetSelectSignal ().AddListener (OnSelected);
		}

		void OnSelected (string name){
			signal.Dispatch (name);
		}

		public override void OnRemove ()
		{
			view.GetSelectSignal ().RemoveListener (OnSelected);
		}
	}
}

