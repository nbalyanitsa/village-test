﻿using System;
using strange.extensions.mediation.impl;
using Signals;
using Views.Api;

namespace Mediators
{
	public class CancelItemPlacementMediator:Mediator
	{
		[Inject]
		public ICancelItemPlacement view{ get; set; }

		[Inject]
		public CancelItemPlacementSignal signal { get; set; }

		public override void OnRegister ()
		{
			view.GetCancelSignal ().AddListener (CancelPlacement);
		}

		public void CancelPlacement (){
			signal.Dispatch ();
		}

		override public void OnRemove (){
			view.GetCancelSignal ().RemoveListener (CancelPlacement);
		}
	}
}

