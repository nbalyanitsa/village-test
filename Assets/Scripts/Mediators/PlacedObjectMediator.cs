﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;
using Models.Api;

namespace Mediators
{
	public class PlacedObjectMediator:Mediator
	{
		[Inject]
		public IDisplayPlacedObject view{ get; set; }

		[Inject]
		public PlacedItemUpdatedSignal placedObjectUpdated{ get; set; }

		[Inject]
		public IReadSelectedItemName selectedItemName{ get; set; }

		[Inject]
		public IReadBuildMenu buildMenu{ get; set; }

		public override void OnRegister ()
		{
			placedObjectUpdated.AddListener (UpdateObj);
		}

		void UpdateObj(){
			var itemName = selectedItemName.GetSelectedtemName ();
			if (itemName == null) {
				view.SetPlacedObject (null);
			} else {
				view.SetPlacedObject (buildMenu.GetItem (itemName) as BuildObject);
			}
		}

		public override void OnRemove ()
		{
			placedObjectUpdated.RemoveListener (UpdateObj);
		}
	}
}

