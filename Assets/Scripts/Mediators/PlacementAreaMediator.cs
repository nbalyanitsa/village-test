﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;
using Models.Api;
using UnityEngine;

namespace Mediators
{
	public class PlacementAreaMediator:Mediator
	{
		[Inject]
		public IDisplayPlacementArea view{ get; set; }

		[Inject]
		public PlacedItemUpdatedSignal placedObjectUpdated{ get; set; }

		[Inject]
		public IReadSelectedItemName selectedItemName{ get; set; }

		[Inject]
		public IReadBuildMenu buildMenu{ get; set; }

		[Inject]
		public IReadHeightMap heightMap{ get; set; }

		[Inject]
		public IReadPlacedObjects placedObjects{ get; set; }

		[Inject]
		public IWritePlacementPosition placementPosition{ get; set; }

		[Inject]
		public ItemPlacedSignal itemPlaced{ get; set; }


		public override void OnRegister ()
		{
			placedObjectUpdated.AddListener (UpdateSize);
			view.GetUpdateSignal ().AddListener (UpdateAvailability);
			itemPlaced.AddListener (OnItemPlaced);
		}

		void UpdateSize(){
			view.SetSize(GetSize());
		}

		void OnItemPlaced(Vector2Int pos){
			UpdateAvailability ();
		}

		void UpdateAvailability (){
			var pos = view.GetPosition ();
			var pos2 = new Vector2Int (pos.x, pos.y);
			var size = GetSize ();
			var deltas = heightMap.GetDeltasFrom (pos2, size);

			var availability = new bool[size.x, size.y];
			for (int x = 0; x < size.x; x++){
				for (int y = 0; y < size.y; y++){
					availability [x, y] = (deltas [x, y] == 0) && placedObjects.IsCellFree(pos2 - new Vector2Int(x, y));
				}
			}
			view.SetAvailabilty (availability);

			placementPosition.SetPlacementPosition (pos2);
		}

		Vector2Int GetSize(){
			var itemName = selectedItemName.GetSelectedtemName ();
			if (itemName == null) {
				return Vector2Int.zero;
			}
			var item = buildMenu.GetItem (itemName) as BuildObject;
			return new Vector2Int(item.width, item.height);
		}

		public override void OnRemove ()
		{
			placedObjectUpdated.RemoveListener (UpdateSize);
			view.GetUpdateSignal ().RemoveListener (UpdateAvailability);
			itemPlaced.RemoveListener (OnItemPlaced);
		}
	}
}

