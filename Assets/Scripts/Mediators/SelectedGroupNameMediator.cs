﻿using strange.extensions.mediation.impl;
using Views.Api;
using Models.Api;
using Signals;

namespace Mediators
{
	public class SelectedGroupNameMediator:Mediator
	{
		[Inject]
		public IRequireSelectedGroupName view{ get; set;}

		[Inject]
		public IReadSelectedGroupName selectedGroup{ get; set;}

		[Inject]
		public PopulateBuildMenuSignal update{ get; set;}

		public override void OnRegister()
		{
			update.AddListener (UpdateView);
		}

		[PostConstruct]
		public void UpdateView(){
			view.SetSelectedGroupName (selectedGroup.GetSelectedGroupName ());
		}

		override public void OnRemove(){
			update.RemoveListener (UpdateView);
		}
	}
}

