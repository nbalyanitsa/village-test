﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Models.Api;
using Signals;
using UnityEngine;

namespace Mediators
{
	public class ItemAvailabilityMediator: Mediator
	{
		[Inject]
		public IDisplayAvailableCount view{ get; set; }

		[Inject]
		public IReadBuildMenu buildMenu{ get; set; }

		[Inject]
		public IReadPlacedObjects placedObjects{ get; set; }

		[Inject]
		public ItemPlacedSignal itemPlaced{ get; set; }

		public override void OnRegister ()
		{
			itemPlaced.AddListener (OnItemPlaced);
			view.GetUpdateSignal ().AddListener (UpdateView);
		}

		void OnItemPlaced(Vector2Int pos){
			UpdateView ();
		}

		void UpdateView (){
			int available = -1; //means no limit;
			var item = buildMenu.GetItem(view.GetName());
			if (item is BuildObject){
				var obj = (item as BuildObject);
				if (obj.maxCount > 0) {
					available = obj.maxCount - placedObjects.GetCountOf (obj.name);
				}
			}

			view.SetCount (available);
		}

		public override void OnRemove ()
		{
			itemPlaced.RemoveListener (OnItemPlaced);
			view.GetUpdateSignal ().RemoveListener (UpdateView);
		}
	}
}

