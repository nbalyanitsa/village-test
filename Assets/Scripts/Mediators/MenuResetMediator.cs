﻿using System;
using strange.extensions.mediation.impl;
using Views.Api;
using Signals;

namespace Mediators
{
	public class MenuResetMediator:Mediator
	{
		[Inject]
		public IResetBuildMenu view {get; set;}

		[Inject]
		public ResetBuildMenuSignal signal { get; set; }

		public override void OnRegister ()
		{
			view.GetResetSignal ().AddListener (OnSelected);
		}

		void OnSelected (){
			signal.Dispatch ();
		}

		public override void OnRemove ()
		{
			view.GetResetSignal ().RemoveListener (OnSelected);
		}
	}
}

