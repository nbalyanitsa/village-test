﻿using System;
using Views.Api;
using Models.Api;
using strange.extensions.mediation.impl;
using Services.Api;

namespace Mediators
{
	public class NamedSpriteMediator:Mediator
	{
		[Inject]
		public IRequireNamedSprite view{ get; set; }

		[Inject]
		public IProvideSprite provider{ get; set; }

		public override void OnRegister ()
		{
			view.GetUpdateSignal ().AddListener (UpdateView);
		}

		void UpdateView (){
			view.SetSprite (provider.GetSprite (new SpriteProvisionContext{name = view.GetName()}));
		}

		public override void OnRemove ()
		{
			view.GetUpdateSignal ().RemoveListener (UpdateView);
		}
	}
}

