﻿using System;
using UnityEngine;

namespace Views.Api
{
	public interface IDisplayMapObject
	{
		Vector3Int GetPosition();
		void SetMapObject(BuildObject value);
	}
}

