﻿using System;
using System.Collections.Generic;

namespace Views.Api
{
	public interface IDisplayBuildMenu
	{
		void ShowBuildMenu(List<NamedEntity> value);
	}
}

