﻿using System;
using UnityEngine;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface IRequireNamedSprite
	{
		Signal GetUpdateSignal();
		string GetName();
		void SetSprite(Sprite value);
	}
}

