﻿using System;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface IDisplayAvailableCount
	{
		string GetName();
		void SetCount(int value);
		Signal GetUpdateSignal();
	}
}

