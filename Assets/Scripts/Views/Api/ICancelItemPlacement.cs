﻿using System;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface ICancelItemPlacement
	{
		Signal GetCancelSignal ();
	}
}

