﻿using System;

namespace Views.Api
{
	public interface IDisplayPlacedObject
	{
		void SetPlacedObject(BuildObject obj);
	}
}

