﻿using System;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface ISelectBuildItem
	{
		Signal<string> GetSelectSignal();
	}
}

