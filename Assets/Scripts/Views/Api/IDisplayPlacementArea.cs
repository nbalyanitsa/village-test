﻿using System;
using UnityEngine;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface IDisplayPlacementArea
	{
		Signal GetUpdateSignal ();

		Vector3Int GetPosition();
		void SetSize (Vector2Int value);
		void SetAvailabilty(bool[,] value);
	}
}

