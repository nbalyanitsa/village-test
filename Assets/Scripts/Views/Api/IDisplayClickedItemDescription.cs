﻿using System;

namespace Views.Api
{
	public interface IDisplayClickedItemDescription
	{
		void ShowDescription(string value);
	}
}

