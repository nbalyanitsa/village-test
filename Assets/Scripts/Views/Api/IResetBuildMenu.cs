﻿using System;
using strange.extensions.signal.impl;

namespace Views.Api
{
	public interface IResetBuildMenu
	{
		Signal GetResetSignal();
	}
}

