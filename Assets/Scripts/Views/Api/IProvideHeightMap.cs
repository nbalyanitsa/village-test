﻿using System;

namespace Views.Api
{
	public interface IProvideHeightMap
	{
		int[,] GetHeigthMap();
	}
}

