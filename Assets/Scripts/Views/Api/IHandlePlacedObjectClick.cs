﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Views.Api
{
	public interface IHandlePlacedObjectClick
	{
		Signal<Vector3Int> GetClickedSignal();
	}
}

