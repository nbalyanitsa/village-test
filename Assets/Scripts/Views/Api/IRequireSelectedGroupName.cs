﻿using System;

namespace Views.Api
{
	public interface IRequireSelectedGroupName
	{
		void SetSelectedGroupName(string value);
	}
}

