﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.UI;
using Views.Api;
using strange.extensions.signal.impl;
using UnityEngine.EventSystems;

public class DisplayBuildItem : View, IRequireNamedSprite, IDisplayAvailableCount, IPointerClickHandler, ISelectBuildItem
{

	public Text titleText;
	public Text countText;

	int _countValue = -1;

	Signal _updateNeeded = new Signal ();
	Signal<string> _selected = new Signal<string> ();

	// Use this for initialization
	override protected void Start () {
		titleText.text = name;
		//could be potentially used in case of name change (not the case in this scope) 
		_updateNeeded.Dispatch ();
	}

	public Signal<string> GetSelectSignal (){
		return _selected;
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (_countValue != 0) {
			_selected.Dispatch (name);
		}
	}

	public Signal GetUpdateSignal (){
		return _updateNeeded;
	}

	public string GetName (){
		return name;
	}

	public void SetSprite (Sprite value){
		GetComponent<Image> ().sprite = value;
	}

	public void SetCount (int value){
		_countValue = value;
		countText.text = value >= 0 ? value.ToString () : "";
	}
}
