﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using Views.Api;

public class FlushHeightMapToModel : View, IProvideHeightMap
{
	public int[,] GetHeigthMap ()
	{
		var generator = GetComponent<GenerateTerrain> ();
		if (generator == null) {
			return null;//nothing to get the data from
		}
		var size = generator.size;
		int[,] result = new int[size.x, size.y];
		for (var x = 0; x < size.x; x++) {
			for (var y = 0; y < size.y; y++) {
				var tile = GetTileAt (x, y, size);
				result [x, y] = tile.GetComponent<DisplayTile> ().GetPosition ().z;
			}
		}
		return result;
	}

	GameObject GetTileAt (int x, int y, Vector2Int size){
		var idx = (x * size.y) + y;
		if (idx < transform.childCount)
			return transform.GetChild (idx).gameObject;
		return null;
	}
}
