﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using Views.Api;
using strange.extensions.signal.impl;

public class DisplayTile : View, IRequireNamedSprite, IDisplayMapObject
{
	public GameObject objectRenderer;

	[SerializeField]
	Vector3Int _pos;

	Signal _update = new Signal ();
	string _name;
	BuildObject _obj;

	protected override void Start ()
	{
		base.Start ();
		_name = "Terrain";
	}

	public Signal GetUpdateSignal (){
		return _update;
	}

	public string GetName (){
		return _name;
	}

	public void SetSprite (Sprite value){
		GetComponent<SpriteRenderer> ().sprite = value;
	}

	public void SetTransform (Vector3Int pos, Vector3 size){
		_pos = pos;
		transform.localPosition = new Vector3 (
			-(_pos.x - _pos.y) * size.x,
			-(_pos.x + _pos.y) * size.y + size.z * _pos.z, 
			(_pos.x + _pos.y) * -0.02f);
	}

	public Vector3Int GetPosition (){
		return _pos;
	}

	public void SetMapObject (BuildObject obj){
		_obj = obj;
		while (transform.childCount > 0) {
			var child = transform.GetChild (0);
			child.parent = null;
			Destroy (child.gameObject);
		}

		if (obj != null) {
			if (obj.type == ObjectType.BUILDING) {
				var renderer = Instantiate (objectRenderer, transform);
				renderer.transform.localPosition = new Vector3 (0f, 0f, -0.01f);
				renderer.name = obj.name;
			} else if (obj.type == ObjectType.TERRAIN){
				_name = obj.name;
				_update.Dispatch ();
			}
		}
	}
}
