﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using UnityEngine.EventSystems;
using strange.extensions.signal.impl;
using Views.Api;

public class HandleTileClick : View, IHandlePlacedObjectClick
{
	Signal<Vector3Int> _clickedAt = new Signal<Vector3Int> ();

	public Signal<Vector3Int> GetClickedSignal(){
		return _clickedAt;
	}

	// Update is called once per frame
	void Update () {
		if (EventSystem.current.IsPointerOverGameObject ())
			return;
		
		if (Application.isMobilePlatform) { 
			return;//a whole additional set of logic needed here for mobile. descoped
		}

		if (!Input.GetMouseButtonDown (0)) {
			return;
		}

		Vector2 worldPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (worldPoint, Vector2.zero);
		if (hit.collider != null) {
				
			var tile = hit.collider.gameObject.GetComponent<DisplayTile> ();
			if (tile != null) {
				_clickedAt.Dispatch (tile.GetPosition ());
			}
		}
	}
}
