﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using strange.extensions.mediation.impl;
using Views.Api;
using strange.extensions.signal.impl;

public class DisplayPlacementArea : View, IDisplayPlacementArea, IPlaceSelectedBuildItem, ICancelItemPlacement
{
	public Sprite tileWireframe;
	public Vector2 tileSize;
	public Color availableColor;
	public Color forbiddenColor;

	Signal _updateNeeded = new Signal ();
	Signal _placeObject = new Signal ();
	Signal _cancel = new Signal ();

	Collider2D _lastHit;
	Vector3Int _tilePosition;
	bool _canPlace;

	public Signal GetUpdateSignal (){
		return _updateNeeded;
	}

	public Signal GetPlacedSignal (){
		return _placeObject;
	}

	public Vector3Int GetPosition (){
		return _tilePosition;
	}

	public Signal GetCancelSignal () {
		return _cancel;
	}

	public void SetSize (Vector2Int value){
		while (transform.childCount > 0) {
			Transform child = transform.GetChild (0);
			Destroy (child.gameObject);
			child.SetParent (null);
		}

		for (int x = 0; x < value.x; x++) {
			for (int y = 0; y < value.y; y++) {
				
				GameObject cell = new GameObject ();

				//duplicates positioning with DisplayTile. Possibly could be reused
				cell.transform.localPosition = 
					new Vector3 (
					(x - y) * tileSize.x,
					(x + y) * tileSize.y, 
					(x + y) * 0.01f);
				cell.transform.SetParent (transform, false);
				
				cell.AddComponent (typeof(SpriteRenderer));
				var spriteRenderer = cell.GetComponent<SpriteRenderer> ();
				spriteRenderer.sprite = tileWireframe;
				spriteRenderer.color = availableColor;
			}
		}
	}

	public void SetAvailabilty (bool[,] value){
		_canPlace = true;
		for (int x = 0; x < value.GetLength (0); x++) {
			for (int y = 0; y < value.GetLength (1); y++) {
				var child = transform.GetChild (x * value.GetLength (1) + y);
				_canPlace &= value [x, y];
				child.GetComponent<SpriteRenderer> ().color = value [x, y] ? availableColor : forbiddenColor;
			}
		}
	}

	void Update () {
		if (EventSystem.current.IsPointerOverGameObject ())
			return;


		if (Application.isMobilePlatform) { 
			return;//a whole additional set of logic needed here for mobile. descoped
		}

		if (Input.GetMouseButtonDown (1)) {
			_cancel.Dispatch ();
			return;
		}
			
		if (Input.GetMouseButton (0) && _canPlace) {
			_placeObject.Dispatch ();
			return;
		}

		if ((Input.GetAxis ("Mouse X") == 0) && (Input.GetAxis ("Mouse Y") == 0)) {
			return;
		}

		Vector2 worldPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast (worldPoint, Vector2.zero);
		if (hit.collider != null && _lastHit != hit.collider) {
			_lastHit = hit.collider;

			var tile = hit.collider.gameObject.GetComponent<DisplayTile> ();
			if (tile != null)
				_tilePosition = tile.GetPosition ();

			transform.position = new Vector3 (
				hit.collider.gameObject.transform.position.x,
				hit.collider.gameObject.transform.position.y,
				hit.collider.gameObject.transform.position.z - 0.0001f
			); 
			_updateNeeded.Dispatch ();
		}
	}
}
