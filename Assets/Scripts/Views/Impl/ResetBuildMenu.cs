﻿using System;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using Views.Api;
using UnityEngine.EventSystems;

public class ResetBuildMenu:View, IPointerClickHandler, IResetBuildMenu, IRequireSelectedGroupName
{
	Signal _reset = new Signal ();

	public void OnPointerClick (PointerEventData eventData)
	{
		_reset.Dispatch ();
	}

	public Signal GetResetSignal (){
		return _reset;
	}

	public void SetSelectedGroupName (string value){
		gameObject.SetActive (value != null);
	}

}

