﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using Views.Api;

public class MouseDrag : View, IDisplayPlacedObject
{

	Vector3 _startPos = Vector3.zero;
	bool _isPlacing;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0) && !_isPlacing) {
			var pos = Input.mousePosition;
			if (_startPos == Vector3.zero) {
				_startPos = Input.mousePosition - transform.position;
			} else {
				transform.position = Input.mousePosition - _startPos;
			}
		} else {
			_startPos = Vector3.zero;
		}
	}

	public void SetPlacedObject(BuildObject obj){
		_isPlacing = obj != null;
	}
}
