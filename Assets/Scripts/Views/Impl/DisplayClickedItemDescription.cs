﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using Views.Api;
using UnityEngine.UI;

public class DisplayClickedItemDescription : View, IDisplayClickedItemDescription {
	
	public Text description;

	// Use this for initialization
	override protected void Start () {
		base.Start ();
		gameObject.SetActive (false);
	}

	public void ShowDescription(string value){
		description.text = value;
		gameObject.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0))
			gameObject.SetActive (false);
	}
}
