﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Views.Api;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

public class DisplayMapObject : View, IRequireNamedSprite {
	
	Signal _updateNeeded = new Signal();

	override protected void Start () {
		//could be potentially used in case of name change (not the case in this scope) 
		_updateNeeded.Dispatch ();
	}

	public Signal GetUpdateSignal(){
		return _updateNeeded;
	}

	public string GetName(){
		return name;
	}

	public void SetSprite(Sprite value){
		GetComponent<SpriteRenderer> ().sprite = value;
	}
}
