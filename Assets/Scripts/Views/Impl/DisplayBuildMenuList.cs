﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.mediation.impl;
using Views.Api;

public class DisplayBuildMenuList : View, IDisplayBuildMenu
{
	public GameObject item;
	public Vector2 itemSize;
	public int padding;
	GameObject _container;

	override protected void Awake (){
		_container = new GameObject ();
		_container.AddComponent (typeof(RectTransform));
		_container.transform.SetParent(transform, false);
		_container.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
	}

	public void ShowBuildMenu (List<NamedEntity> value){
		GetComponent<RectTransform> ().sizeDelta = new Vector2 (itemSize.x * value.Count + padding, itemSize.y);

		for (int i = 0; i < _container.transform.childCount; i++) {
			Transform child = _container.transform.GetChild (i);
			Destroy (child.gameObject);
		}

		var offset = ((value.Count - 1) / 2f); //center the list horisontally
		for (var i = 0; i < value.Count; i++) {
			var newItem = Instantiate (item, _container.transform);
			newItem.name = value [i].name;
			newItem.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (itemSize.x * (i - offset), 0);
		}
	}
}
