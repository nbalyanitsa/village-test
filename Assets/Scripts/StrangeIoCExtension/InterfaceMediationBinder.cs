﻿//nick.balyanitsa: this is a copy of strange.extensions.mediation.impl.MediationBinder
//does the same thig, but also iterates interfaces imlemented in view class (line 61) to find the corresponding mapping, 
//allowing to mediate abstractions instead of conctrete classes.

using System;
using System.Collections;
using UnityEngine;
using strange.extensions.injector.api;
using strange.extensions.mediation.api;
using strange.framework.api;
using strange.framework.impl;
using strange.extensions.mediation.impl;

public class InterfaceMediationBinder : Binder, IMediationBinder
{

	[Inject]
	public IInjectionBinder injectionBinder{ get; set;}

	public InterfaceMediationBinder ()
	{
	}

	public override IBinding GetRawBinding ()
	{
		return new MediationBinding (resolver) as IBinding;
	}

	public void Trigger(MediationEvent evt, IView view)
	{
		switch(evt)
		{
		case MediationEvent.AWAKE:
			mapView (view);
			break;
		case MediationEvent.DESTROYED:
			unmapView (view);
			break;
		default:
			break;
		}
	}

	new public IMediationBinding Bind<T> ()
	{
		return base.Bind<T> () as IMediationBinding;
	}

	public IMediationBinding BindView<T>() where T : MonoBehaviour
	{
		return base.Bind<T> () as IMediationBinding;
	}

	/// Creates and registers one or more Mediators for a specific View instance.
	/// Takes a specific View instance and a binding and, if a binding is found for that type, creates and registers a Mediator.
	virtual protected void mapView(IView view)
	{
		Type viewType = view.GetType();
		mapViewAsType (view, viewType);

		foreach (Type implemented in viewType.GetInterfaces()){
			mapViewAsType (view, implemented);
		}
	}

	virtual protected void mapViewAsType(IView view, Type viewType){
		IMediationBinding binding = GetBinding (viewType) as IMediationBinding;
		if (binding == null) {
			return;
		}
		if (bindings.ContainsKey(viewType))
		{
			object[] values = binding.value as object[];
			int aa = values.Length;
			for (int a = 0; a < aa; a++)
			{
				MonoBehaviour mono = view as MonoBehaviour;
				Type mediatorType = values [a] as Type;
				if (mediatorType == viewType)
				{
					throw new MediationException(viewType + "mapped to itself. The result would be a stack overflow.", MediationExceptionType.MEDIATOR_VIEW_STACK_OVERFLOW);
				}
				MonoBehaviour mediator = mono.gameObject.AddComponent(mediatorType) as MonoBehaviour;
				if (mediator == null)
					throw new MediationException ("The view: " + viewType.ToString() + " is mapped to mediator: " + mediatorType.ToString() + ". AddComponent resulted in null, which probably means " + mediatorType.ToString().Substring(mediatorType.ToString().LastIndexOf(".")+1) + " is not a MonoBehaviour.", MediationExceptionType.NULL_MEDIATOR);
				if (mediator is IMediator)
					((IMediator)mediator).PreRegister ();
				
				injectionBinder.Bind (viewType).ToValue (view).ToInject(false);
				injectionBinder.injector.Inject (mediator);
				injectionBinder.Unbind(viewType);
				if (mediator is IMediator)
					((IMediator)mediator).OnRegister ();
			}
		}
	}

	virtual protected void unmapView(IView view)
	{
		Type viewType = view.GetType();
		unmapViewAsType (view, viewType);

		foreach (Type implemented in viewType.GetInterfaces()){
			unmapViewAsType (view, implemented);
		}
	}

	virtual protected void unmapViewAsType(IView view, Type viewType){
		IMediationBinding binding = GetBinding (viewType) as IMediationBinding;
		if (binding == null) {
			return;
		}

		if (bindings.ContainsKey(viewType))
		{
			object[] values = binding.value as object[];
			int aa = values.Length;
			for (int a = 0; a < aa; a++)
			{
				Type mediatorType = values[a] as Type;
				MonoBehaviour mono = view as MonoBehaviour;
				IMediator mediator = mono.GetComponent(mediatorType) as IMediator;
				if (mediator != null)
				{
					mediator.OnRemove();
				}
			}
		}
	}

	private void enableView(IView view)
	{
	}

	private void disableView(IView view)
	{
	}
}


