﻿/// A Signal for Starting the Context

using System;
using strange.extensions.signal.impl;

namespace Signals
{
	public class StartSignal : Signal
	{
	}
}