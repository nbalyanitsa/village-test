﻿using System;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Signals
{
	public class ItemPlacedSignal:Signal<Vector2Int>
	{
		public ItemPlacedSignal ()
		{
		}
	}
}

