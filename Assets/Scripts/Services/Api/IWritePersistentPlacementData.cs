﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Services.Api
{
	public interface IWritePersistentPlacementData
	{
		void SaveData(string name, Dictionary<Vector2Int, BuildObject> data);
	}
}

