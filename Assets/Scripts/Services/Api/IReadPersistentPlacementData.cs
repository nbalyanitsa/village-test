﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Services.Api
{
	public interface IReadPersistentPlacementData
	{
		Dictionary<Vector2Int, BuildObject> LoadData (string name);
	}
}

