using System;
using Views.Api;
using Models.Api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Services.Api
{
	public struct SpriteProvisionContext {
		public string name;
		//things like heigthmap etc could be used to determine the proper sprite for road object 
	}

	public interface IProvideSprite
	{
		Sprite GetSprite (SpriteProvisionContext context);
	}

}

