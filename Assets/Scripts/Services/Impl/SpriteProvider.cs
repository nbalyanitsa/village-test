﻿using System;
using Services.Api;
using UnityEngine;
using System.Collections.Generic;

namespace Services.Impl
{
	public class SpriteProvider:IProvideSprite
	{
		Dictionary<String, Sprite> _sprites;

		public SpriteProvider ()
		{
			_sprites = new Dictionary<String, Sprite> ();
			foreach (Sprite sprite in Resources.LoadAll<Sprite> ("")) {
				_sprites.Add (sprite.name, sprite);
			}
		}

		public Sprite GetSprite (SpriteProvisionContext context)
		{

			//this is obviously is supposed to be externalised and to have only a nice name matching loop
			//yuck
			switch (context.name) {
			case ("Advanced Buildings"):
				return _sprites ["buildings_5"];

			case ("Arcade"):
				return _sprites ["buildings_6"];

			case ("Car wash"):
				return _sprites ["buildings_7"];

			case ("Drugstore"):
				return _sprites ["buildings_8"];

			case ("Forum"):
				return _sprites ["buildings_9"];

			case ("Hotel"):
				return _sprites ["buildings_10"];

			case ("Market"):
				return _sprites ["buildings_11"];

			case ("Pharmacy"):
				return _sprites ["buildings_12"];

			case ("Skyscraper"):
				return _sprites ["buildings_13"];

			case ("Shop"):
				return _sprites ["buildings_14"];

			case ("Supermarket"):
				return _sprites ["buildings_15"];

			case ("Warehouse"):
				return _sprites ["buildings_16"];

			case ("Restaurant"):
				return _sprites ["buildings_17"];

			case ("Office"):
				return _sprites ["buildings_18"];

			case ("Hairdressers"):
				return _sprites ["buildings_19"];

			case ("Common Buildings"):
				return _sprites ["buildings_0"];

			case ("House #1"):
				return _sprites ["buildings_0"];

			case ("House #2"):
				return _sprites ["buildings_1"];

			case ("House #3"):
				return _sprites ["buildings_2"];

			case ("House #4"):
				return _sprites ["buildings_3"];

			case ("House #5"):
				return _sprites ["buildings_4"];

			case ("Road"):
				return _sprites ["terrain_14"];
			
			case ("Terrain"):
				return _sprites ["terrain_0"];
			}
			return null;
		}
	}
}















