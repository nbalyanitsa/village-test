﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;
using Services.Api;
using System.Collections.Generic;

namespace Services.Impl
{
	[Serializable]
	struct PlacementInfo
	{
		public int x;
		public int y;
		public BuildObject obj;
	}

	public class PersistentPlacementDataService : IReadPersistentPlacementData, IWritePersistentPlacementData
	{
		public void SaveData (string name, Dictionary<Vector2Int, BuildObject> data)
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/" + name);

			var array = new PlacementInfo[data.Keys.Count];

			var i = 0;
			foreach (var key in data.Keys) {
				array [i] = new PlacementInfo{ x = key.x, y = key.y, obj = data [key] };
				i++;
			}

			bf.Serialize (file, array);
			file.Close ();

			#if UNITY_WEBGL && !UNITY_EDITOR
			Application.ExternalEval("FS.syncfs(false, function (err) {})");
			#endif
		}

		public Dictionary<Vector2Int, BuildObject> LoadData (string name)
		{
			if (!File.Exists (Application.persistentDataPath + "/" + name))
				return null;
			FileStream file = File.OpenRead (Application.persistentDataPath + "/" + name);

			BinaryFormatter bf = new BinaryFormatter ();
			PlacementInfo[] list = null;
			if (file.Length != 0) {
				list = (PlacementInfo[])bf.Deserialize (file);
			}
			file.Close ();

			var data = new Dictionary<Vector2Int, BuildObject> ();
			if (list != null) {
				foreach (PlacementInfo e in list) {
					var key = new Vector2Int (e.x, e.y);
					if (!data.ContainsKey (key))
						data.Add (key, e.obj);
				}
			}
			return data;
		}
	}
}

