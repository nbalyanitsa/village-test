﻿using System;
using strange.extensions.command.impl;
using Models.Api;
using Signals;

namespace Commands
{
	public class ResetBuildMenuCommand:Command
	{
		[Inject]
		public IWriteSelectedGroupName selectedGroup{ get; set; }

		[Inject]
		public PopulateBuildMenuSignal populateMenu{ get; set; }

		public override void Execute ()
		{
			selectedGroup.SetSelectedGroupName (null);
			populateMenu.Dispatch ();
		}
	}
}

