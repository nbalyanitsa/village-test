﻿using System;
using strange.extensions.command.impl;
using Models.Api;
using Signals;

namespace Commands
{
	public class CancelPlacementCommand:Command
	{
		[Inject]
		public IWriteSelectedItemName selectedItemWriter{ get; set; }

		[Inject]
		public PlacedItemUpdatedSignal placedItemUpdated{ get; set; }

		public override void Execute ()
		{
			selectedItemWriter.SetSelectedItemName (null);
			placedItemUpdated.Dispatch ();
		}
	}
}

