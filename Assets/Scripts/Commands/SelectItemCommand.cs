﻿using System;
using strange.extensions.command.impl;
using Models.Api;
using Signals;

namespace Commands
{
	public class SelectItemCommand:Command
	{
		[Inject]
		public IReadBuildMenu buildMenu{ get; set; }

		[Inject]
		public IWriteSelectedGroupName selectedGroup{ get; set; }

		[Inject]
		public IWriteSelectedItemName selectedItem{ get; set; }

		[Inject]
		public PopulateBuildMenuSignal populateMenu{ get; set; }

		[Inject]
		public PlacedItemUpdatedSignal placedObjectUpdated{ get; set; }

		[Inject]
		public String name{ get; set; }

		public override void Execute ()
		{
			var item = buildMenu.GetItem (name);
			if (item is NamedGroup) {
				selectedGroup.SetSelectedGroupName (name);
				populateMenu.Dispatch ();
			}else if (item is BuildObject) {
				selectedGroup.SetSelectedGroupName (null);
				populateMenu.Dispatch ();

				selectedItem.SetSelectedItemName (name);
				placedObjectUpdated.Dispatch ();
			}
		}
	}
}

