﻿using System;
using Models.Api;
using Signals;
using strange.extensions.command.impl;
using Services.Api;

namespace Commands
{
	public class PlaceItemCommand:Command
	{
		[Inject]
		public IReadBuildMenu buildMenu{ get; set; }

		//a need to inject both read and write interfaces probably means i should also map the model as entire class 
		//and use in commands like this (leaving interfaces for more narrow usecases)
		[Inject]
		public IReadSelectedItemName selectedItemReader{ get; set; }

		[Inject]
		public IWriteSelectedItemName selectedItemWriter{ get; set; }

		[Inject]
		public IReadPlacementPosition position{ get; set; }

		[Inject]
		public IReadPlacedObjects placedObjectsReader{ get; set; }

		[Inject]
		public IWritePlacedObjects placedObjectsWriter{ get; set; }

		[Inject]
		public ItemPlacedSignal itemPlaced{ get; set; }

		[Inject]
		public PlacedItemUpdatedSignal placedItemUpdated{ get; set; }

		[Inject]
		public IWritePersistentPlacementData persistentStorage{ get; set; }

		public override void Execute ()
		{
			var selectedItemName = selectedItemReader.GetSelectedtemName ();
			if (selectedItemName == null)
				return;
			var item = buildMenu.GetItem (selectedItemName);
			if (item is BuildObject) {
				var obj = item as BuildObject;
				var available = -1;//no limit
				if (obj.maxCount > 0) {
					available = obj.maxCount - placedObjectsReader.GetCountOf (obj.name);
				}

				var pos = position.GetPlacementPosition ();
				if ((available != 0) && placedObjectsWriter.Add (pos, obj)) {
					itemPlaced.Dispatch (pos);

					persistentStorage.SaveData("map", placedObjectsReader.GetData());

					//probably decrement could worki just fine
					available = obj.maxCount - placedObjectsReader.GetCountOf (obj.name);
				}

				if (available == 0) {
					selectedItemWriter.SetSelectedItemName (null);
					placedItemUpdated.Dispatch ();
				}
			}
		}
	}
}

