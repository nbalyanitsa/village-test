﻿using System;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using Services.Api;
using Models.Api;

public class StartCommand : Command
{
	
	[Inject]
	public IReadPersistentPlacementData persistentStorage{ get; set; }

	[Inject]
	public IWritePlacedObjects placedObjects{ get; set; }

	public override void Execute ()
	{
		var data = persistentStorage.LoadData ("map");
		if (data != null) {
			foreach (var pos in data.Keys) {
				if (data [pos] != null)
					placedObjects.Add (pos, data [pos]);
			}
		}
	}
}

