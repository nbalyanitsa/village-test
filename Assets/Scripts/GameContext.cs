﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.impl;
using strange.extensions.context.api;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.mediation.api;
using Models.Api;
using Signals;
using Views.Api;
using Mediators;
using Services.Impl;
using Commands;
using Models.Impl;
using Services.Api;

public class GameContext : MVCSContext
{
	
	public GameContext (MonoBehaviour view) : base (view)
	{
	}

	public GameContext (MonoBehaviour view, ContextStartupFlags flags) : base (view, flags)
	{
	}

	// Unbind the default EventCommandBinder and rebind the SignalCommandBinder
	protected override void addCoreComponents ()
	{
		base.addCoreComponents ();
		injectionBinder.Unbind<ICommandBinder> ();
		injectionBinder.Bind<ICommandBinder> ().To<SignalCommandBinder> ().ToSingleton ();

		injectionBinder.Unbind<IMediationBinder> ();
		injectionBinder.Bind<IMediationBinder> ().To<InterfaceMediationBinder> ().ToSingleton ();
	}

	// Override Start so that we can fire the StartSignal
	override public IContext Start ()
	{
		base.Start ();
		StartSignal startSignal = (StartSignal)injectionBinder.GetInstance<StartSignal> ();
		startSignal.Dispatch ();
		return this;
	}

	protected override void mapBindings ()
	{
		//models
		injectionBinder.Bind<IReadBuildMenu> ().ToValue(new BuildMenuModel ());

		//same value, 2 interface injections to separate writers and readers. 
		//singleton doesn't seem to work in this case 
		var selectedGroupNameModel = new SelectedGroupNameModel ();
		injectionBinder.Bind<IReadSelectedGroupName> ().ToValue(selectedGroupNameModel);
		injectionBinder.Bind<IWriteSelectedGroupName> ().ToValue(selectedGroupNameModel);

		var selectedItemNameModel = new SelectedItemNameModel ();
		injectionBinder.Bind<IReadSelectedItemName> ().ToValue(selectedItemNameModel);
		injectionBinder.Bind<IWriteSelectedItemName> ().ToValue(selectedItemNameModel);

		var placementPositionModel = new PlacementPositionModel ();
		injectionBinder.Bind<IReadPlacementPosition> ().ToValue(placementPositionModel);
		injectionBinder.Bind<IWritePlacementPosition> ().ToValue(placementPositionModel);

		var heigthMapModel = new HeightMapModel ();
		injectionBinder.Bind<IReadHeightMap> ().ToValue(heigthMapModel);
		injectionBinder.Bind<IWriteHeightMap> ().ToValue(heigthMapModel);

		var placedObjects = new PlacedObjectsModel ();
		injectionBinder.Bind<IReadPlacedObjects> ().ToValue(placedObjects);
		injectionBinder.Bind<IWritePlacedObjects> ().ToValue(placedObjects);

		//external stuff (eg storage, library access, etc) goes here
		injectionBinder.Bind<IProvideSprite> ().ToValue(new SpriteProvider ());

		//external stuff (eg storage, library access, etc) goes here
		var service = new PersistentPlacementDataService ();
		injectionBinder.Bind<IReadPersistentPlacementData> ().ToValue(service);
		injectionBinder.Bind<IWritePersistentPlacementData> ().ToValue(service);


		//mediation
		mediationBinder.Bind<IDisplayBuildMenu> ().To<BuildMenuMediator> ();
		mediationBinder.Bind<IDisplayMapObject> ().To<MapObjectMediator> ();
		mediationBinder.Bind<IRequireNamedSprite> ().To<NamedSpriteMediator> ();
		mediationBinder.Bind<IDisplayAvailableCount> ().To<ItemAvailabilityMediator> ();
		mediationBinder.Bind<ISelectBuildItem> ().To<ItemSelectionMediator> ();
		mediationBinder.Bind<IDisplayPlacedObject> ().To<PlacedObjectMediator> ();
		mediationBinder.Bind<IPlaceSelectedBuildItem>().To<ItemPlacementMediator> ();
		mediationBinder.Bind<IResetBuildMenu> ().To<MenuResetMediator> ();
		mediationBinder.Bind<IRequireSelectedGroupName> ().To<SelectedGroupNameMediator> ();
		mediationBinder.Bind<IDisplayPlacementArea> ().To<PlacementAreaMediator> ();
		mediationBinder.Bind<ICancelItemPlacement> ().To<CancelItemPlacementMediator> ();
		mediationBinder.Bind<IHandlePlacedObjectClick> ().To<PlacedObjectClickMediator> ();
		mediationBinder.Bind<IDisplayClickedItemDescription> ().To<ClickedItemDescriptionMediator> ();
		mediationBinder.Bind<IProvideHeightMap> ().To<HeightMapMediator> ();


		//global notifications
		injectionBinder.Bind<PopulateBuildMenuSignal> ().ToSingleton ();
		injectionBinder.Bind<PlacedItemUpdatedSignal> ().ToSingleton ();
		injectionBinder.Bind<ItemPlacedSignal> ().ToSingleton ();
		injectionBinder.Bind<PlacedObjectClicked> ().ToSingleton ();

		commandBinder.Bind<StartSignal> ().To<StartCommand> ().Once ();
		commandBinder.Bind<SelectItemSignal> ().To<SelectItemCommand> ();
		commandBinder.Bind<PlaceItemSignal> ().To<PlaceItemCommand> ();
		commandBinder.Bind<ResetBuildMenuSignal> ().To<ResetBuildMenuCommand> ();
		commandBinder.Bind<CancelItemPlacementSignal> ().To<CancelPlacementCommand> ();

	}
}
