﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

#if UNITY_EDITOR

using UnityEditor;

[CustomEditor (typeof(GenerateTerrain))]
public class GenerateTerrainGUI : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		GenerateTerrain script = (GenerateTerrain)target;
		if (GUILayout.Button ("Generate")) {
			script.Recreate ();
		}
	}
}
#endif

[ExecuteInEditMode]
public class GenerateTerrain : MonoBehaviour
{
	public Vector3 tileSize;
	public Vector2Int size;
	public Texture2D atlas;
	public GameObject prefab;

	[Range (0u, 10u)]
	public int relief = 3;

	[Range (1u, 8u)]
	public int maxHeight = 5;

	[Range (0.1f, 1f)]
	public float slopeRatio = 0.6f;


	#if UNITY_EDITOR
	private int[,] _heightMap;

	public void Recreate (){

		if (size.x < 1 || size.y < 1) {
			Debug.LogError ("Map dimensions should be positive!");
		}

		_heightMap = new int[size.x, size.y];

		for (var i = 0; i < relief; i++) {
			var posX = Random.Range (0, size.x);
			var posY = Random.Range (0, size.y);

			_heightMap [posX, posY] = maxHeight;
			SmoothOut (posX, posY);
		}

		Sprite[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath ("Assets/Resources/" + atlas.name + ".png").OfType<Sprite> ().ToArray ();

		while (transform.childCount > 0) {
			Transform child = transform.GetChild (0);
			child.parent = null;
			DestroyImmediate (child.gameObject);
		}

		for (var x = 0; x < size.x; x++) {
			for (var y = 0; y < size.y; y++) {

				var tile = Instantiate (prefab, transform);
				tile.GetComponent<DisplayTile> ().SetTransform (new Vector3Int(x, y, _heightMap [x, y]), tileSize);
				tile.GetComponent<SpriteRenderer>().sprite = sprites[0];//set sprite directly as IoC is not available in edit mode
				tile.name = prefab.name + "@" + x + ":" + y;
			}
		}

		//whatever been put during the play session should be wiped as might conflict with new height map
		File.Delete (Application.persistentDataPath + "/map");
	}

	void SmoothOut (Vector2Int point){
		SmoothOut (point.x, point.y);
	}

	void SmoothOut (int x, int y){
		var toSmoothOut = new List<Vector2Int> ();
		for (var offX = x - 1; offX <= x + 1; offX++) {
			for (var offY = y - 1; offY <= y + 1; offY++) {
				if (offX < 0 || offY < 0 || offX >= _heightMap.GetLength (0) || offY >= _heightMap.GetLength (1))
					continue;
				
				var diff = _heightMap [x, y] - _heightMap [offX, offY];
				if (diff > 1) {
					_heightMap [offX, offY] = _heightMap [x, y];
					if ((Random.Range (0, 1f) < slopeRatio) || Mathf.Abs (x - offX + y - offY) == 2) {
						_heightMap [offX, offY] -= 1;
					}
					toSmoothOut.Add (new Vector2Int (offX, offY));
				}
			}
		}

		foreach (Vector2Int point in toSmoothOut) {
			SmoothOut (point);
		}
	}
	#endif
}