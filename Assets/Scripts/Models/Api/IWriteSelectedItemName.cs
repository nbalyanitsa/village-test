﻿using System;

namespace Models.Api
{
	public interface IWriteSelectedItemName
	{
		void SetSelectedItemName(string value);
	}
}

