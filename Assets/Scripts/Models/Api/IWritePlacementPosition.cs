﻿using System;
using UnityEngine;

namespace Models.Api
{
	public interface IWritePlacementPosition
	{
		void SetPlacementPosition (Vector2Int pos);
	}
}

