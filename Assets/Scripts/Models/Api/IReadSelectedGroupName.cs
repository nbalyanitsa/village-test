﻿using System;

namespace Models.Api
{
	public interface IReadSelectedGroupName
	{
		string GetSelectedGroupName();
	}
}

