﻿using System;

namespace Models.Api
{
	public interface IWriteHeightMap
	{
		void SetHeightMap(int[,] value);
	}
}

