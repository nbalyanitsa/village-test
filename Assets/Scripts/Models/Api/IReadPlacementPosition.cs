﻿using System;
using UnityEngine;

namespace Models.Api
{
	public interface IReadPlacementPosition
	{
		Vector2Int GetPlacementPosition ();
	}
}

