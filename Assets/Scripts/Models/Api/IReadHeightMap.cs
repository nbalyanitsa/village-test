﻿using System;
using UnityEngine;

namespace Models.Api
{
	public interface IReadHeightMap
	{
		int[,] GetDeltasFrom(Vector2Int pos, Vector2Int size);
	}
}

