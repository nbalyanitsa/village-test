﻿using System;
using System.Collections.Generic;

namespace Models.Api
{
	public interface IReadBuildMenu
	{
		List<NamedEntity> GetBuildMenu ();
		NamedEntity GetItem (string name);
		string GetItemDescription (string name);
	}
}

