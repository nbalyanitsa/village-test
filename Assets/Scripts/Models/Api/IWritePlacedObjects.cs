﻿using System;
using UnityEngine;

namespace Models.Api
{
	public interface IWritePlacedObjects
	{
		bool Add(Vector2Int pos, BuildObject obj);
	}
}

