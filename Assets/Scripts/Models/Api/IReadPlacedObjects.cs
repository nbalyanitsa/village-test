﻿using System;
using UnityEngine;
using System.Collections.Generic;

namespace Models.Api
{
	public interface IReadPlacedObjects
	{
		Dictionary<Vector2Int, BuildObject> GetData();
		bool IsCellFree(Vector2Int pos);
		BuildObject GetObjectAt(Vector2Int pos);
		int GetCountOf(string name);
		float GetLastPlacementTime();
	}
}

