﻿using System;

namespace Models.Api
{
	public interface IWriteSelectedGroupName
	{
		void SetSelectedGroupName(string value);
	}
}

