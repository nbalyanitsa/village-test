﻿using System;
using UnityEngine;
using Models.Api;

namespace Models.Impl
{
	public class PlacementPositionModel:IWritePlacementPosition, IReadPlacementPosition
	{
		Vector2Int _pos;

		public void SetPlacementPosition (Vector2Int pos)
		{
			_pos = pos;
		}

		public Vector2Int GetPlacementPosition ()
		{
			return _pos;
		}
	}
}

