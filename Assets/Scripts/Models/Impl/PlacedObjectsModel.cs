﻿using System;
using UnityEngine;
using Models.Api;
using System.Collections.Generic;
using System.Linq;

namespace Models.Impl
{
	public class PlacedObjectsModel:IWritePlacedObjects, IReadPlacedObjects
	{
		Dictionary<Vector2Int, BuildObject> _objects = new Dictionary<Vector2Int, BuildObject> ();
		List<Vector2Int> _occupiedCells = new List<Vector2Int> ();
		float _lastPlacementTime;

		public Dictionary<Vector2Int, BuildObject> GetData(){
			return _objects;
		}

		public bool Add (Vector2Int pos, BuildObject obj){
			if (_objects.ContainsKey (pos))
				return false;

			_lastPlacementTime = Time.time;
			_objects.Add (pos, obj);

			for (int x = 0; x < obj.width; x++) {
				for (int y = 0; y < obj.height; y++) {
					_occupiedCells.Add (pos - new Vector2Int (x, y));
				}
			}
			return true;
		}

		public bool IsCellFree (Vector2Int pos){
			return !_occupiedCells.Contains (pos);
		}

		public float GetLastPlacementTime(){
			return _lastPlacementTime;
		}

		public BuildObject GetObjectAt (Vector2Int pos){
			if (_objects.ContainsKey (pos))
				return _objects [pos];
			
			return null;
		}

		public int GetCountOf(string name){
			return _objects.Values.Where((BuildObject arg) => arg.name == name).Count();
		}
	}
}

