﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Models.Api;
using System;

public enum ObjectType
{
	TERRAIN,
	BUILDING
}

[Serializable]
public abstract class NamedEntity
{
	public string name;
	public string desc;
}

[Serializable]
public class NamedGroup:NamedEntity
{
	public List<NamedEntity> objects;
}

[Serializable]
public class BuildObject:NamedEntity
{
	public ObjectType type;
	public int width;
	public int height;
	public int maxCount;
}

public class BuildMenuModel:IReadBuildMenu //a setter could be e.g. an external configuration loader
{
	//this is supposed to be externalised, but out of scope atm
	List<NamedEntity> _data;
	Dictionary<string, NamedEntity> _lookup;
	Dictionary<string, NamedEntity> _parents;

	public BuildMenuModel ()
	{
		//should be habdled by a service
		SetData (new List<NamedEntity> {
			new NamedGroup {
				name = "Advanced Buildings",
				objects = new List<NamedEntity> {
					new BuildObject {
						name = "Arcade",
						desc = "Arcade: Play amazing games here",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Drugstore",
						desc = "Drugstore: Get a medecine or two",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Forum",
						desc = "Forum: Speak your mind!",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Hotel",
						desc = "Hotel: Get a room!",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Market",
						desc = "Market: Spend money get stuff",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Pharmacy",
						desc = "Pharmacy: Another place to buy a cough syrup",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Skyscraper",
						desc = "Skyscraper: Try not using lifts!",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Shop",
						desc = "Shop: Fancy things for big bucks",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Supermarket",
						desc = "Supermarket: Too bad its far from home",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Warehouse",
						desc = "Warehouse: Store things",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Restaurant",
						desc = "Restaurant: Eat stuff here",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Office",
						desc = "Office: Work (or at least pretend)",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					},
					new BuildObject {
						name = "Hairdressers",
						desc = "Hairdressers: Careful with those scissors!",
						type = ObjectType.BUILDING,
						width = 2, height = 2,
						maxCount = 1
					}
				}
			},
			new NamedGroup {
				name = "Common Buildings",
				desc = "Just a house",
				objects = new List<NamedEntity> {
					new BuildObject {
						name = "House #1",
						type = ObjectType.BUILDING,
						width = 1, height = 1
					},
					new BuildObject {
						name = "House #2",
						type = ObjectType.BUILDING,
						width = 1, height = 1,
					},
					new BuildObject {
						name = "House #3",
						type = ObjectType.BUILDING,
						width = 1, height = 1,
						maxCount = 3
					},
					new BuildObject {
						name = "House #4",
						type = ObjectType.BUILDING,
						width = 1, height = 1,
						maxCount = 5
					},
					new BuildObject {
						name = "House #5",
						type = ObjectType.BUILDING,
						width = 1, height = 1,
						maxCount = 8
					}
				}
			},
			new BuildObject {
				name = "Road",
				desc = "Sorry it doesn't bend nicely :(",
				type = ObjectType.TERRAIN,
				width = 1, height = 1
			}
		});
	}

	public List<NamedEntity> GetBuildMenu (){
		return _data;
	}

	public NamedEntity GetItem (string name){
		return _lookup [name];
	}

	public string GetItemDescription (string name){
		var obj = GetItem (name);
		while (obj != null) {
			if (obj.desc != null) {
				return obj.desc;
			}
			obj = FindParent (obj);
		}

		return null;
	}

	NamedEntity FindParent (NamedEntity obj){
		if (_parents.ContainsKey (obj.name)) {
			return _parents [obj.name];
		}
		return null;
	}

	public void SetData (List<NamedEntity> data){
		_data = data;

		_lookup = new Dictionary<string, NamedEntity> ();
		_parents = new Dictionary<string, NamedEntity> ();
		PopulateLookup (data, null);
	}

	void PopulateLookup (List<NamedEntity> data, NamedEntity parent){
		foreach (NamedEntity e in data) {
			if (parent != null) {
				_parents.Add (e.name, parent);
			}
			_lookup.Add (e.name, e);
			if (e is NamedGroup) {
				PopulateLookup ((e as NamedGroup).objects, e);
			}
		}
	}
}