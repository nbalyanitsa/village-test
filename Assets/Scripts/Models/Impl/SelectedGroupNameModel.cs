﻿using System;
using Models.Api;

namespace Models.Impl
{
	public class SelectedGroupNameModel:IReadSelectedGroupName, IWriteSelectedGroupName
	{
		string _selectedGroupName;

		public string GetSelectedGroupName(){
			return _selectedGroupName;
		}

		public void SetSelectedGroupName(string value){
			_selectedGroupName = value;
		}
	}
}

