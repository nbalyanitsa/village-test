﻿using System;
using Models.Api;

namespace Models.Impl
{
	public class SelectedItemNameModel:IReadSelectedItemName, IWriteSelectedItemName
	{
		string _selectedItemName;

		public string GetSelectedtemName(){
			return _selectedItemName;
		}

		public void SetSelectedItemName(string value){
			_selectedItemName = value;
		}
	}
}

