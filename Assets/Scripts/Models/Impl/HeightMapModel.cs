﻿using System;
using UnityEngine;
using Models.Api;

namespace Models.Impl
{
	public class HeightMapModel:IReadHeightMap, IWriteHeightMap
	{
		int[,] _data;

		public void SetHeightMap (int[,] value){
			_data = value;
		}

		public int[,] GetDeltasFrom (Vector2Int pos, Vector2Int size){
			var result = new int[size.x, size.y];
			for (int i = 0; i < size.x; i++) {
				for (int j = 0; j < size.y; j++) {

					var deltaPos = pos + new Vector2Int(-i, -j);

					//todo utils, or something
					if (deltaPos.x >= 0 && deltaPos.y >= 0 && deltaPos.x < _data.GetLength (0) && deltaPos.y < _data.GetLength (1)) {
						result [i, j] = _data [deltaPos.x, deltaPos.y] - _data [pos.x, pos.y];
					} else {
						//this actually equals the case of tile being below the one at pos, which is semantically wrong. 
						//a special value should be introduced here meaning the cell is out of map bounds. 
						//but as soon as we dont want to have our buildongs on non flat surfaces - this should work for now (preventing the building being placed on the map partially).
						result [i, j] = -1;
					}
				}
			}
			return result;
		}
	}
}

